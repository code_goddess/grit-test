function uniqueZeroSet(arr) {
    let result = [];
    let unique = [];
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {

        for(let j = i + 1; j < arr.length; j++) {
            for(let k = j + 1; k < arr.length; k++) {
                let element = [];
                sum = arr[i] + arr[j] + arr[k]
                if(sum === 0) {
                    if(!(unique.includes(arr[i]) && unique.includes(arr[j]) && unique.includes(arr[k]))) {
                        element.push(arr[i], arr[j], arr[k]);
                        result.push(element);
                        unique.push(arr[i], arr[j], arr[k]);
                    }
                }
            }
        }
    }
    return result
}