let capitalize = (str) => {
    return str[0].toUpperCase() + str.slice(1)
}

let buildUI = (array) => {
    const dropdown = document.querySelector(".dropdown-menu");
    const container = document.querySelector("#container")
    array.forEach(item => {
        let {name, picture} = item
        let link = document.createElement("a");
        link.classList.add("dropdown-item")
        link.textContent = name.first;
        link.href = "user.html"

        link.addEventListener("click", function(e) {
            e.preventDefault();
            console.log(name.first);
            // window.location.href ="user.html"
            container.innerHTML= `<div class="user-info">
                                        <div class="img-box">
                                            <img src="${picture.large}">
                                        </div>
                                        <div>
                                            <p id="name">${capitalize(name.title)} ${capitalize(name.first)} ${capitalize(name.last)}</p>
                                        </div>
                                    </div>`;
            // window.location.href ="user.html"
        })
        dropdown.appendChild(link)
    })
}




(() => {
    let url = "https://randomuser.me/api/?results=100";
    let male;
    let female;
    const container = document.querySelector("#container")
    fetch(url)
    .then(response => response.json())
    .then(data=> {
        console.log(data)
        return data
    })
    .then(({results})=> {
        console.log(results)
        male = results.filter(user => user.gender === "male").length
        female = results.filter(user => user.gender === "female").length;
        anychart.onDocumentReady(function() {

            var data = [
                {x: "Female", value: female},
                {x: "Male", value: male},
            ];

            container.innerHTML = '';

            var chart = anychart.pie();

            // set the chart title
            chart.title("Pie Chart of the gender of 100 random users");

            // add the data
            chart.data(data);

            // display the chart in the container
            chart.container('container');
            chart.draw();
            document.querySelector(".anychart-credits").remove()

        });
        buildUI(results)
    })
    .catch(err => console.log(err))
})();

